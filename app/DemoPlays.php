<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoPlays extends Model
{
    protected $table = 'play_by_play';
    public $timestamps = false;
    
    protected $fillable = [
        'playerid',
        'round',
        'play_count',
        'attacker',
        'assister',
        'headshot',
        'weapon',
        'weapon_fauxitemid',
        'weapon_itemid',
        'weapon_originalowner_xuid',
        'team_id',
        'demo_id'
	];
}
