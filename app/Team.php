<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'first_half',
        'second_half',
        'total',
        'demo_id'
	];
}
