<?php

namespace App\Http\Controllers\Docs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

class DocControllerVersionOne extends Controller
{ 

    public function __construct(){
        $this->middleware('auth');
        $this->data = array(
            'open_menus' => array('docs-v1')
        );
    }

    public function index(){
        return view('docs/v1/overview')->with($this->data);
    }

    public function demo(){
        return view('docs/v1/demo')->with($this->data);
    }

    public function players(){
        return view('docs/v1/players')->with($this->data);
    }
    
    public function teams(){
        return view('docs/v1/teams')->with($this->data);
    }
}
