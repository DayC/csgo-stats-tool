<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Demo;

class PagesController extends Controller
{ 

    public function __construct(){
 
    }

    // Redirect index to the dashboard
    public function index(){
        $auth = Auth::user();
        if($auth){
            return redirect()->route('dashboard');
        }else{
            return view('auth/login');
        }
    }

    // Display the account 
    public function account(){
        return view('pages/account');
    }

    // Show API Token
    public function apiToken(){
        $user = Auth::user();
        return response()->json(["token" => $user->api_token], 201);
    }

    /* INVESTIGATE - Old function to see individual file */
    /*public function file($id){
        if($id){
            $data = array(
                'file_id' => $id
            );
            return view('pages/file')->with($data);
        }else{
            return view('pages/file');
        }
    }*/

    // Display list of demos
    public function demos(int $count = 0){
        $user = Auth::user();
        $demo_count = Demo::where('user_id', '=', $user->id)->count();
        $limit = 5;


        $data = Demo::where('user_id', '=', $user->id)
                ->limit($limit)
                ->offset($count == 0 ? 0 : ($count - 1) * $limit)
                ->get();
        
        return view('pages/demos')->with(
            [
                'data' => $data, 
                'pages' => $demo_count > 0 ? round($demo_count / $limit) : 0,
                'url' => route('demos'),
                'empty' => $data->isEmpty()
            ]
        );
    }

    // Display single demo stats
    public function demo(int $id){
        $demo = Demo::where('demo.id', '=', $id)->get();
        
        return view('pages/demo')->with([
            'demo' => $demo,
            'empty' => $demo->isEmpty()
        ]);
    }
}
