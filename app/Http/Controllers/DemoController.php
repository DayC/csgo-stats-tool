<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Demo;
use App\DemoData;
use App\DemoPlays;
use App\Team;
use App\Rounds;
use App\Players;
use App\GunsFired;
use App\GrenadesThrown;

class DemoController extends Controller {
   public function __construct(){
        $this->middleware('auth');
    }

    public function createDemo(Request $request){
        $user_id = Auth::id();
        $file_data = $request->input('params');
        $file_name = $file_data["server_name"] . ' ' . $file_data["client_name"] . ' ' . $file_data["map_name"];
        $player_teams = array();

        if($user_id >= 1){
            $stamp = $this->createDemoStamp($file_name);
            $demo = Demo::create(['stamp' => $stamp, 'user_id' => $user_id]);
            if($demo->id > 0){
                $demo_data = DemoData::create([
                    'client' => $file_data["client_name"], 
                    'map' => $file_data["map_name"], 
                    'server' => $file_data["server_name"], 
                    'demo_id' => $demo->id
                ]);
                
                // Populate Team Models
                if(is_array($file_data["teams"])){           
                    foreach($file_data["teams"] as $team){
                        $teamObj = Team::create([
                            'name' => $team["team_name"],
                            'first_half' => $team["scores"]["first_half"],
                            'second_half' => $team["scores"]["second_half"],
                            'total' => $team["scores"]["total"],
                            'demo_id' => $demo->id
                        ]);
                        
                        if($teamObj->id > 0 && is_array($team["rounds"])){
                            $rounds = array();
                            foreach($team["rounds"] as $round){
                                $rounds[] = array(
                                    'round' => $round["round_count"],
                                    'score' => $round["round_score"],
                                    'side' => $round["side"],
                                    'overtime' => $round["overtime"],
                                    'team_id' => $teamObj->id,
                                    'demo_id' => $demo->id
                                );
                            }
                            Rounds::insert($rounds);
                        }

                        // Assign a team id to each player id
                        if($teamObj->id > 0 && is_array($team["members"])){
                            foreach($team["members"] as $member){
                                $player_teams[$member] = $teamObj->id;
                            }
                        }
                    }
                }

                // Populate Play models
                if($file_data["play_by_play"]){
                    $playbyplay = array();
                    foreach($file_data["play_by_play"] as $round => $plays){
                        if($plays){
                            foreach($plays as $playcount => $play){
                                if(isset($player_teams[$play["userid"]])){
                                    $team_id = $player_teams[$play["userid"]];
                                }else{
                                    $team_id = 0;
                                }
                                $playbyplay[] = array(
                                    'playerid' => $play["userid"],
                                    'round' => $round,
                                    'play_count' => $playcount,
                                    'attacker' => $play["attacker"],
                                    'assister' => $play["assister"],
                                    'headshot' => $play["headshot"],
                                    'weapon' => $play["weapon"],
                                    'weapon_fauxitemid' => $play["weapon_fauxitemid"],
                                    'weapon_itemid' => $play["weapon_itemid"],
                                    'weapon_originalowner_xuid' => $play["weapon_originalowner_xuid"],
                                    'team_id' => $team_id,
                                    'demo_id' => $demo->id,
                                );
                            }
                        }
                    }
                    DemoPlays::insert($playbyplay);
                }

                // Populate player models
                if(is_array($file_data["player_data"])){
                    foreach($file_data["player_data"] as $player){
                        if(isset($player_teams[$player["user_id"]])){
                            $team_id = $player_teams[$player["user_id"]];
                        }else{
                            $team_id = 0;
                        }
                        $playerObj = Players::create([
                            'name' => $player["name"],
                            'player_id' => $player["user_id"],
                            'guid' => $player["guid"],
                            'kills' => $player["kills"],
                            'deaths' => $player["deaths"],
                            'assists' => $player["assists"],
                            'team_id' => $team_id,
                            'demo_id' => $demo->id
                        ]);

                        // Populate the player's GunsFired model
                        if($player["guns_fired"]){
                            foreach($player["guns_fired"] as $gun => $amount){
                                $gunObj = GunsFired::create([
                                    'weapon' => $gun,
                                    'amount' => $amount,
                                    'player_id' => $playerObj->id,
                                    'demo_id' => $demo->id,
                                ]);
                            }
                        }
                        // Populate the player's GrenadesThrown model
                        if($player["grenades_thrown"]){
                            foreach($player["grenades_thrown"] as $grenade => $amount){
                                $grenadeObj = GrenadesThrown::create([
                                    'grenade' => $grenade,
                                    'amount' => $amount,
                                    'player_id' => $playerObj->id,
                                    'demo_id' => $demo->id,
                                ]);
                            }
                        }


                    }
                }

                return response()->json(["message" => "Demo was created"], 201);
            }else{
                return response()->json(["message" => "Demo couldn't be created"], 400);
            }
        }else{
            return response()->json(["message" => "User needs to be logged in"], 400);
        }
    }

    private function createDemoStamp($file_name = ''){
        return base64_encode($file_name);
    }
}
