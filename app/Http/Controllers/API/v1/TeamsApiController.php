<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use App\DemoPlays;
use App\Team;
use App\Rounds;

class TeamsApiController extends Controller
{
    protected $request;

    public function __construct(Request $request){
        $this->middleware('auth:api');
        $this->request = $request;
    }

    public function getTeamsByDemoId(){
        $user = $this->request->user();
        $demo_id = $this->request->demo_id;
        $teams = array();
        if($demo_id > 0){
            $teams = Team::where('demo_id', '=', $demo_id)
                        ->select('id', 'name', 'first_half', 'second_half', 'total')
                        ->get();
        }
        return response()->json($teams, 200);
    }

    public function getTeamRounds(){
        $demo_id = $this->request->demo_id;
        
        if(!is_array($this->request->team_ids)){
            $team_ids = explode(',', str_replace(' ', '', $this->request->team_ids));
        }else{
            $team_ids = $this->request->team_ids;
        }
        
        $rounds = array();

        if($demo_id > 0 && is_array($team_ids)){
            $round_data = Rounds::where('demo_id', '=', $demo_id)
                        ->whereIn('team_id', $team_ids)
                        ->select('id', 'round', 'score', 'side', 'overtime', 'team_id')
                        ->get();
        }

        if(count($round_data) > 0){
            foreach($round_data as $data){
                $round = $data["round"];
                $temp_data = $data;
                $rounds[$round][] = $temp_data;
            }
        }

        return response()->json($rounds, 200);
    }

    public function getPlays(){
        $demo_id = $this->request->demo_id;
        if(!is_array($this->request->team_ids)){
            $team_ids = explode(',', str_replace(' ', '', $this->request->team_ids));
        }else{
            $team_ids = $this->request->team_ids;
        }
        
        $plays = array();
        if($demo_id > 0 && is_array($team_ids)){
            $play_data = DemoPlays::where('demo_id', '=', $demo_id)
                        ->whereIn('team_id', $team_ids)
                        ->get();
        }

        if(count($play_data) > 0){
            foreach($play_data as $data){
                $round = $data["round"];
                $play_count = $data["play_count"];
                $temp_data = $data;
                $plays[$round][$play_count] = $temp_data;
            }
        }

        return response()->json($plays, 200);
    }
}
