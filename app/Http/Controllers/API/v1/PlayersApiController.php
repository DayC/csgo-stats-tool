<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Players;
use App\GunsFired;
use App\GrenadesThrown;

class PlayersApiController extends Controller
{
    protected $request;

    public function __construct(Request $request){
        $this->middleware('auth:api');
        $this->request = $request;
    }

    public function getPlayersByDemoId(){
        $user = $this->request->user();
        $demo_id = $this->request->demo_id;
        $players = array();

        if($demo_id > 0){
            (array) $players = Players::where('demo_id', '=', $demo_id)->get();
        }
        
        if($players){
            foreach($players as $key => $player){
               $players[$key]["guns_fired"] = $this->getGunsFired($player["id"], $demo_id);
               $players[$key]["grenades_thrown"] = $this->getGrenadesThrown($player["id"], $demo_id);
            }
        }
        return response()->json($players, 200);
    }

    public function getPlayersById(){
        $user = $this->request->user();
        $demo_id = $this->request->demo_id;
        if(!is_array($this->request->ids)){
            $ids = explode(',', str_replace(' ', '', $this->request->ids));
        }else{
            $ids = $this->request->ids;
        }
        $players = array();

        if($demo_id > 0){
           (array) $players = Players::where('demo_id', '=', $demo_id)
                        ->whereIn('id', $ids)
                        ->get();
        }

        if($players){
            foreach($players as $key => $player){
               $players[$key]["guns_fired"] = $this->getGunsFired($player["id"], $demo_id);
               $players[$key]["grenades_thrown"] = $this->getGrenadesThrown($player["id"], $demo_id);
            }
        }

        return response()->json($players, 200);
    }

    public function getPlayersByPlayerId(){
        $user = $this->request->user();
        $demo_id = $this->request->demo_id;
        if(!is_array($this->request->player_ids)){
            $player_ids = explode(',', str_replace(' ', '', $this->request->player_ids));
        }else{
            $player_ids = $this->request->player_ids;
        }
        $players = array();

        if($demo_id > 0){
            (array) $players = Players::where('demo_id', '=', $demo_id)
                        ->whereIn('player_id', $player_ids)
                        ->get();
        }

        if($players){
            foreach($players as $key => $player){
               $players[$key]["guns_fired"] = $this->getGunsFired($player["id"], $demo_id);
               $players[$key]["grenades_thrown"] = $this->getGrenadesThrown($player["id"], $demo_id);
            }
        }

        return response()->json($players, 200);
    }

    private function getGunsFired($player_id, $demo_id){
        $guns = array();

        if($player_id){
            (array) $guns = GunsFired::where('demo_id', '=', $demo_id)
                            ->where('player_id', '=', $player_id)
                            ->select('weapon', 'amount')
                            ->get();
        }

        return $guns;
    }

    private function getGrenadesThrown($player_id, $demo_id){
        $grenades = array();

        if($player_id){
            (array) $grenades = GrenadesThrown::where('demo_id', '=', $demo_id)
                            ->where('player_id', '=', $player_id)
                            ->select('grenade', 'amount')
                            ->get();
        }

        return $grenades;
    }
}
