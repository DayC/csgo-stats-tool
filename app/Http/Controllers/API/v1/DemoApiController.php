<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Demo;
use App\DemoData;
use App\DemoPlays;
use App\Team;
use App\Rounds;

class DemoApiController extends Controller
{
    protected $request;

    public function __construct(Request $request){
        $this->middleware('auth:api');
        $this->request = $request;
    }

    public function getDemos(){
        $user = $this->request->user();
        $data = Demo::where('demo.user_id', '=', $user->id)
                    ->join('file_meta', 'demo.id', '=', 'file_meta.demo_id')
                    ->select('demo.id', 'stamp as name', 'created_at as created', 'client', 'map', 'server')
                    ->get();
        $data = json_decode(json_encode($data), true);
        $demos = $this->decodeValues($data);

        return response()->json($demos, 200);
    }

    public function getDemoByName(){
        $user = $this->request->user();
        $data = array();
        if(isset($this->request->name) && $this->request->name !== ''){
            $stamp = base64_encode($this->request->name);
            $data = Demo::where('stamp', 'LIKE', '%'.$stamp.'%')
                                ->where('demo.user_id', '=', $user->id)
                                ->join('file_meta', 'demo.id', '=', 'file_meta.demo_id')
                                ->select('demo.id', 'stamp as name', 'created_at as created', 'client', 'map', 'server')
                                ->take(1)->get();
        }
        $data = json_decode(json_encode($data), true);
        $demos = $this->decodeValues($data);

        return response()->json($demos, 200);
    }

    public function getDemoById(Request $request){
        $user = $this->request->user();
        $data = array();
        if(isset($request->id) && $request->id > 0){
            $data = Demo::where('demo.id', '=', $request->id)
                                ->where('demo.user_id', '=', $user->id)
                                ->join('file_meta', 'demo.id', '=', 'file_meta.demo_id')
                                ->select('demo.id', 'stamp as name', 'created_at as created', 'client', 'map', 'server')
                                ->take(1)->get();
        }

        $data = json_decode(json_encode($data), true);
        $demos = $this->decodeValues($data);

        return response()->json($demos, 200);
    }

    private function decodeValues($data){
        $demos = array();
        if($data){
            foreach($data as $demo){
                foreach($demo as $key => $value){
                    
                    switch($key){
                        case "name":
                             $value = base64_decode($value);
                             $demos[$key] = $value;
                        break;
                        default:
                            $demos[$key] = $value;
                    }
                    $demos[$key] = $value;
                }
            }
        }
        return $demos;
    }
}
