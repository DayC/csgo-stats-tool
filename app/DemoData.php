<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoData extends Model
{
    protected $table = 'file_meta';
    public $timestamps = false;
    
    protected $fillable = [
		'client', 
		'map',
		'server',
		'demo_id'
	];
}
