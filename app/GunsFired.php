<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GunsFired extends Model
{
    protected $table = 'guns_fired';
    public $timestamps = false;
    
    protected $fillable = [
        'weapon',
        'amount',
        'player_id',
        'demo_id',
	];
}
