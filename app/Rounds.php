<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rounds extends Model
{
    protected $table = 'rounds';
    public $timestamps = false;
    
    protected $fillable = [
        'round',
        'score',
        'side',
        'overtime',
        'team_id',
        'demo_id'
	];
}
