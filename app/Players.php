<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
    protected $table = 'players';
    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'player_id',
        'guid',
        'kills',
        'deaths',
        'assists',
        'team_id',
        'demo_id',
	];
}
