<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
	protected $table = 'demo';
	public $timestamps = true;

	protected $fillable = [
		'stamp', 
		'user_id',
	];
}
