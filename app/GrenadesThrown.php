<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrenadesThrown extends Model
{
    protected $table = 'grenades_thrown';
    public $timestamps = false;
    
    protected $fillable = [
        'grenade',
        'amount',
        'player_id',
        'demo_id',
	];
}
