<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrenadesThrown extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grenades_thrown', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grenade', 255);
            $table->integer('amount')->default(0);
            $table->integer('player_id');
            $table->bigInteger('demo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
