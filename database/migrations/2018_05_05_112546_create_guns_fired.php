<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGunsFired extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guns_fired', function (Blueprint $table) {
            $table->increments('id');
            $table->string('weapon', 255);
            $table->integer('amount')->default(0);
            $table->integer('player_id');
            $table->bigInteger('demo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
