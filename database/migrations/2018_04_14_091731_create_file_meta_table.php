<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_meta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client', 255)->nullable();
            $table->string('map', 255)->nullable();
            $table->string('server', 255)->nullable();
            $table->bigInteger('demo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_meta');
    }
}
