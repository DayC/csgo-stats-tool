<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayByPlayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('play_by_play', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('round')->nullable();            
            $table->integer('play_count')->nullable();
            $table->integer('playerid')->nullable();            
            $table->integer('attacker')->nullable();
            $table->integer('assister')->nullable();
            $table->boolean('headshot')->nullable();
            $table->string('weapon', 255)->nullable();
            $table->string('weapon_fauxitemid', 255)->nullable();
            $table->string('weapon_itemid', 255)->nullable();
            $table->string('weapon_originalowner_xuid', 255)->nullable();
            $table->bigInteger('team_id')->default(0);
            $table->bigInteger('demo_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('play_by_play');
    }
}
