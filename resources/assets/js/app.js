import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Dashboard from './src/Dashboard';
import Account from './src/Account';

if (document.getElementById('dashboard') !== null) {
    ReactDOM.render(<Dashboard />, document.getElementById('dashboard'));
}

if (document.getElementById('account') !== null){
    ReactDOM.render(<Account />, document.getElementById('account'));
}

//$('.collapse').collapse();