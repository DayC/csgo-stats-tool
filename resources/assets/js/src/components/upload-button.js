import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import $ from 'jquery';

/* An example React component */
class UploadButton extends Component {
    constructor(props) {
        super(props);
        this.showUpload = this.showUpload.bind(this);
    }
    showUpload(){
        let classes = this.props.className.split(" ");
        
        if(classes.indexOf("disabled") < 0){
            $('#upload-input').click();
        }
    }

    render() {
        return (
            <button className={this.props.className} type="button" onClick={this.showUpload}>{this.props.value}</button>
        );
    }
}

UploadButton.propTypes = {
    value: PropTypes.string,
    className: PropTypes.string
}


export default UploadButton;