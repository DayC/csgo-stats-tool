import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

var vendor_path = '../../../../../public/js/vendor/';
var demo = require('../../../../../public/js/vendor/demofile');
window.Buffer = require('../../../../../public/js/vendor/buffer/').Buffer;

/* An example React component */
class FileHandler extends Component {
	constructor(props) {
		super(props);

		// Reader		
		this.reader = new FileReader();

		// File data
		this.file_name = '';
		this.file_id;
		this.demo_file;
		this.file_data = {
			"map_name": "",
			"client_name": "",
			"server_name": "",
			"teams": {},
			"player_data": {},
			"play_by_play": []
		};
		this.teams;
		this.progress_count = 0;

		// Bing handler methods
		this.progress = this.progress.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.demoStart = this.demoStart.bind(this);
		this.demoEnd = this.demoEnd.bind(this);
		this.updatePlayerData = this.updatePlayerData.bind(this);
		this.updateTeamRoundData = this.updateTeamRoundData.bind(this);
		this.updateKillData = this.updateKillData.bind(this);
		this.updateItemData = this.updateItemData.bind(this);

		// Bind assistive methods
		this.fileName = this.fileName.bind(this);
		this.fileID = this.fileID.bind(this);
		this.teamData = this.teamData.bind(this);
		this.teamRoundData = this.teamRoundData.bind(this);
		this.base64_encode = this.base64_encode.bind(this);
		this.base64_decode = this.base64_decode.bind(this);
	}

	componentDidUpdate(){
		if(this.props.file !== null && this.props.file_status === "inactive"){
			// Start of file output
			this.progress(this.progress_count, "File being read...");

			// Reset file data
			this.file_data = {
				"map_name": "",
				"client_name": "",
				"server_name": "",
				"teams": {},
				"player_data": {},
				"play_by_play": []
			};

			// Load the file reader
			this.reader = new FileReader();
			this.reader.onload = () => this.onLoad();

			// Read the file buffer into memory
			this.reader.readAsArrayBuffer(this.props.file);
		}
	}

	/* *
	 * Handler methods
	 * */ 

	// Reader load handler - Primary demofile calls are here.
	onLoad(){
		// Setup our demofile
		this.demo_file = new demo.DemoFile();
		
		// Tell the user our file has been read
		this.progress_count = 10;
		this.progress(this.progress_count, "File read!");
		
		// Do initial demo file data setup
		this.demo_file.on('start', () => this.demoStart());
		
		// Populate the player information
		this.demo_file.stringTables.on('update', e => this.updatePlayerData(e));
		
		// Populate purchase data
		this.demo_file.gameEvents.on('weapon_fire', e => this.updateItemData(e));

		// Populate team data
		this.demo_file.gameEvents.on('round_officially_ended', e => this.updateTeamRoundData(e));

		// Populate kill data
		this.demo_file.gameEvents.on('player_death', e => this.updateKillData(e));

		// Create data
		this.demo_file.entities.on('create', e => this.createPlayerData(e));

		// End of file, send response back.
		this.demo_file.on('end', () => this.demoEnd());

		// Parse the reader's result.
		this.demo_file.parse(new Uint8Array(this.reader.result));
	}

	// Start handler
	demoStart(){
		// Set file name and id
		this.file_name = this.fileName(this.demo_file);
		this.file_id = this.fileID(this.file_name);
		this.file_data["map_name"] = this.demo_file.header.mapName;
		this.file_data["client_name"] = this.demo_file.header.clientName;
		this.file_data["server_name"] = this.demo_file.header.serverName;

		// Update progress indicator
		this.progress_count = 20;
		this.progress(this.progress_count, "Header information created.");	
	}

	// End handler
	demoEnd() {
		/*** Finish end handler ***/
		// Update progress indicator
		this.progress_count = 100;
		this.props.onUpdateFileData(this.file_data);
		this.progress(this.progress_count, "Demo upload complete!", "inactive");
	}

	updatePlayerData(e){
		
		// Make sure we're looking at the right table and that entries exist.
		if (e.name === 'userinfo') {						
			var user_info = e.entries;
			
			user_info.forEach((element, index) => {
				// If there's no user data, skip to next element
				if(element.userData === null){ return; }
				if(element.userData.fakePlayer === true){ return; }

				// If the user isn't a bot then add data to object.
				var user_data = element.userData;
				if (user_data.guid !== "BOT") {
					this.file_data["player_data"][user_data.userId] = {};

					// Only fill data if nothing exists of that user id.
					if (Object.keys(this.file_data["player_data"][user_data.userId]).length <= 0) {
						this.file_data["player_data"][user_data.userId] = {
							"name": user_data.name,
							"user_id": user_data.userId,
							"guid": user_data.guid,
							"team_name": '',
							"kills": 0,
							"deaths": 0,
							"assists": 0,
							"guns_fired":{},
							"grenades_thrown": {
								"flashbang": 0,
								"hegrenade": 0,
								"decoy": 0,
								"incgrenade": 0,
								"smokegrenade": 0
							}
						};
					}
				}
			});
			
			// Update progress indicator
			this.progress_count = 30;
			this.progress(this.progress_count, "Player information extracted.");
		}
	}

	// Set team data
	updateTeamRoundData(e){	
		let teams = this.demo_file.teams;
		let round = (this.demo_file.gameRules.roundNumber - 1);
		let phase = this.demo_file.gameRules.phase;
		let ts = teams[demo.TEAM_TERRORISTS];
		let cts = teams[demo.TEAM_CTS];
		let ts_clan_encode = this.base64_encode(ts.clanName);
		let cts_clan_encode = this.base64_encode(cts.clanName);
		let overtime = false;
		if (ts.clanName === '' || cts.clanName === ''){ return; }

		/**
		 * Setup T data
		*/		
		if (typeof this.file_data["teams"][ts_clan_encode] === "undefined") {
			// Assign initial team data
			this.file_data["teams"][ts_clan_encode] = this.teamData(ts.clanName, ts.score, ts.scoreFirstHalf, ts.scoreSecondHalf, ts.members);

			// Set blank array once so we can find length for round count.
			this.file_data["teams"][ts_clan_encode]["rounds"] = [];
		}
		
		// Check if round data already exists, if so we're in OT
		if (typeof this.file_data["teams"][ts_clan_encode]["rounds"][round] !== "undefined") {
			overtime = true;
		}else{
			overtime = false;
		}

		// Push our data onto the array.
		this.file_data["teams"][ts_clan_encode]["rounds"].push(this.teamRoundData(round, ts.score, "t", overtime));
		this.file_data["teams"][ts_clan_encode]["scores"] = {
			"total": ts.score,
			"first_half": ts.scoreFirstHalf,
			"second_half": ts.scoreSecondHalf
		}

		/**
		 * Setup CT data
		*/
		// Assign initial team data - check to see if it exists first
		if (typeof this.file_data["teams"][cts_clan_encode] === "undefined") {
			this.file_data["teams"][cts_clan_encode] = this.teamData(cts.clanName, cts.score, cts.scoreFirstHalf, cts.scoreSecondHalf, cts.members);
			this.file_data["teams"][cts_clan_encode]["rounds"] = [];
		}

		// Check if round data already exists, if so we're in OT
		if (typeof this.file_data["teams"][cts_clan_encode]["rounds"][round] !== "undefined") {
			overtime = true;
		} else {
			overtime = false;
		}

		// Push our data onto the array.
		this.file_data["teams"][cts_clan_encode]["rounds"].push(this.teamRoundData(round, cts.score, "ct", overtime));
		this.file_data["teams"][cts_clan_encode]["scores"] = {
			"total": cts.score,
			"first_half": cts.scoreFirstHalf,
			"second_half": cts.scoreSecondHalf
		}

		// Update progress per round
		this.progress_count += 1;
		let msg = "Round " + round + " extracted.";
		//if (overtime === true) { msg = "Overtime " + round + " extracted."; }
		this.progress(this.progress_count, msg);
	}

	updateKillData(e){
		let round = this.demo_file.gameRules.roundNumber;		
		if (typeof this.file_data["play_by_play"][(round - 1)] === "undefined") { this.file_data["play_by_play"][(round - 1)] = []; }
		let next_play = this.file_data["play_by_play"][(round - 1)].length;
		this.file_data["play_by_play"][(round - 1)][next_play] = e;

		// Increment player kills
		if (typeof this.file_data["player_data"][e.attacker] !== 'undefined'){
			this.file_data["player_data"][e.attacker]["kills"] += 1;
		}

		// Increment player deaths
		if (typeof this.file_data["player_data"][e.userid] !== 'undefined') {			
			this.file_data["player_data"][e.userid]["deaths"] += 1;
		}

		// Increment player assists
		if (typeof this.file_data["player_data"][e.assister] !== 'undefined' && e.assister > 0) {
			this.file_data["player_data"][e.assister]["assists"] += 1;
		}

	}

	updateItemData(e){
		if (typeof this.file_data["player_data"][e.userid] === 'undefined'){ return; }
		var weapon = e.weapon.split("weapon_").pop();
		// Check to see if current weapon being used is a grenade
		if (this.file_data["player_data"][e.userid]["grenades_thrown"].hasOwnProperty(weapon)) { 
			// If so increment
			this.file_data["player_data"][e.userid]["grenades_thrown"][weapon] += 1;
		} else if (this.file_data["player_data"][e.userid]["guns_fired"].hasOwnProperty(weapon)){
			this.file_data["player_data"][e.userid]["guns_fired"][weapon] += 1;
		}else{
			this.file_data["player_data"][e.userid]["guns_fired"][weapon] = 0;
		}
	}

	createPlayerData(e){
		// We're only interested in player entities being created.
		if (e.entity.serverClass.name !== 'CCSPlayer') { return; }
		var user_data = e.entity;
		if (typeof this.file_data["player_data"][user_data.userId] !== 'object'){
			this.file_data["player_data"][user_data.userId] = {};
			this.file_data["player_data"][user_data.userId] = {
				"name": user_data.name,
				"user_id": user_data.userId,
				"guid": user_data.steamId,
				"team_name": '',
				"kills": 0,
				"deaths": 0,
				"assists": 0,
				"guns_fired": {},
				"grenades_thrown": {
					"flashbang": 0,
					"hegrenade": 0,
					"decoy": 0,
					"incgrenade": 0,
					"smokegrenade": 0
				}
			};
		}
		
	}

	/* *
	 * Assistive methods
	 * */

	// Create a file name and return
	fileName(demo_file){
		let file_name = demo_file.header.serverName + " - " + demo_file.header.clientName + " - " + demo_file.header.mapName + " - " + demo_file.header.playbackTicks;
		return file_name;
	}

	// Create a file id, based on the file name and return
	fileID(file_name){
		let file_id = new Buffer(file_name).toString('base64');
		return file_id;
	}

	// Update the user on our progress
	progress(progress = 0, message = '', file_status = 'active'){
		this.props.onUpdateProgress(progress, message, file_status);
	}

	// Initial team setup
	teamData(clan, score, score_first_half, score_second_half, members){
		
		let team_data = {
			"team_name": clan,
			"rounds": [],
			"scores": {
				"total": score,
				"first_half": score_first_half,
				"second_half": score_second_half
			},
			"members": []
		}

		members.forEach((player, index) => {
			team_data["members"].push(player.userId);
			if(typeof this.file_data["player_data"][player.userId] !== 'undefined'){
				this.file_data["player_data"][player.userId]["team_name"] = clan;
			}
		});

		return team_data;
	}

	// Setup team data
	teamRoundData(round, score, side, overtime){
		let team_data = {
			"round_count": round,
			"round_score": score,
			"side": side,
			"overtime": overtime
		};
		return team_data;
	}

	base64_encode(str) {
		return window.btoa(unescape(encodeURIComponent(str)));
	}

	base64_decode(str) {
		return decodeURIComponent(escape(window.atob(str)));
	}

	render() {
		return (
			<div className="status" style={{ display: this.props.status_display }}>
				<p>{this.props.status}</p>
			</div>
		);
	}
}

export default FileHandler;