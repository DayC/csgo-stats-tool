import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class ProgressBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="upload-progress" style={{ display: this.props.progress_display }}>
        <p className="percentage"><span>{this.props.progress}</span>%</p>
        <div className="progress">
          <div className="progress-bar" role="progressbar" style={{ width:this.props.progress + '%' }}></div>
        </div>
      </div>
    );
  }
}

export default ProgressBar;