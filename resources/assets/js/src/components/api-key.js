import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class ApiKey extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="api-key">
                <label>API Key:</label>
                <pre><code>{this.props.token}</code></pre>
            </div>
        );
    }
}

export default ApiKey;