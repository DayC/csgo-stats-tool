import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import UploadButton from './components/upload-button';
import FileHandler from './components/file-upload';
import ProgressBar from './components/progress-bar';

import $ from 'jquery';
import axios from 'axios'; // Import Axios HTTP client for requests

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			file: null,
			file_status: "inactive",
			progress: 0,
			status: "",
			status_display: "none",
			progress_display: "none",
			btn_class: "btn waves-effect waves-light file-upload-btn",
			btn_value: "Upload File",
		};
		
		this.executeUpload = this.executeUpload.bind(this);
		this.createFormPayload = this.createFormPayload.bind(this);
		this.updateProgress = this.updateProgress.bind(this);
		this.updateFileData = this.updateFileData.bind(this);
		this.updateButtonData = this.updateButtonData.bind(this);
		this.sendData = this.sendData.bind(this);
	}

	executeUpload(event){
		// Set our file and payload
		var file = event.target.files[0];
		var file_ext = '';

		if (typeof file === "undefined"){ return false; }

		file_ext = file.name.split('.').pop();
		if(file_ext === 'dem'){
			this.setState({ 
				file: file, 
				progress_display: "block", 
				btn_class: "btn disabled waves-effect waves-light file-upload-btn",
				btn_value: "File Uploading..."
			});
		}
	}

	createFormPayload(file){
		// create a FormData object which will be sent as the data payload in the AJAX request
		var formData = new FormData();
		
		// add the files to formData object for the data payload
		formData.append('uploads[]', file, file.name);
		
		// Return payload to uploader
		return formData;
  }
  
	updateProgress(progress, status, file_status) {
		// If we've finished with the file then remove the disabled button state and change button text back.
		if (progress === 100 && file_status === "inactive"){
			this.setState({ 
				progress: progress, 
				status: status, 
				file_status: file_status,
				btn_class: "btn waves-effect waves-light file-upload-btn",
				btn_value: "Upload File"
			});
		}else{
			this.setState({ progress: progress, status: status, file_status: file_status });
		}

		// Show status messages
		if (progress === 0 && file_status === "active") {
			this.setState({ status_display: "block" });
		}
  }

  updateFileData(file_data){
	  	this.sendData('/create-demo', file_data);
		
		// Unset our initial file data to avoid recursive interpretations
		this.setState({ file: null });
	}
	
	updateButtonData(btn_class, btn_value){
		this.setState({ btn_class: btn_class, btn_value: btn_value });
	}

	sendData(endpoint, data){
		console.log(data);
		axios.post(endpoint, {
			params: data,
			responseType: 'json',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		}).then(function (response) {
			// Do something here..
			console.log(response.data);
		}).catch(function (error) {
			console.log(error.response);
		});
	}

	render() {
		return (

			<div className="file-upload-form d-flex flex-column justify-content-center">
				<div className=""><h1>Upload a file</h1></div>
				<FileHandler 
					file={this.state.file} 
					file_status={this.state.file_status} 
					status={this.state.status} 
					status_display={this.state.status_display} 
					onUpdateFileData={this.updateFileData} 
					onUpdateProgress={this.updateProgress} 
				/>
				
				<ProgressBar progress={this.state.progress} status={this.state.status} progress_display={this.state.progress_display} />

				<form action="/upload" method="POST">
					<input id="upload-input" type="file" name="uploads[]" multiple="multiple" onChange={this.executeUpload} />
					<UploadButton className={this.state.btn_class} value={this.state.btn_value} onUpdateButtonData={this.updateButtonData} />
				</form>
			</div>
		);
	}
}

export default Dashboard;