import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import ApiKey from './components/api-key';

import axios from 'axios'; // Import Axios HTTP client for requests

class Account extends Component {
	constructor(props) {
		super(props);

		this.state = {
			api_token: ''
		};

		// Bind class functions
		this.fetchToken = this.fetchToken.bind(this);

		// We need to fetch the api token
		this.fetchToken();
	}

	fetchToken(){
		var $account = this;
		axios.get('/account/token')
			.then(response => {
				$account.setState({ api_token: response.data.token });
			});
	}

	render() {
		return (
			<div className="col-12">
				<h1>Account</h1>
				<ApiKey token={this.state.api_token} />
			</div>
		);
	}
}

export default Account;