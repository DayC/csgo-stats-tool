@extends('layouts/app')
@section('content')
<div id="overview" class="col-9 body-content">
    <h1>Overview</h1>
    <p>The TeqR Demo API is a cloud based statistical analytics tool for Counter Strike: Global Offensive. The goal of this tool is to offer a more accessible product analytics product to those looking to improve their CS game.</p>
    <p>This tool belongs to the Representational State Transfer (REST) architectural style and its API allow you to perform 'RESTful' operations using GET, POST, PUT and DELETE actions.</p>
    <h2>What API commands does the TeqR API use?</h2>
    <p>All requests are JSON over HTTP and use the following:</p>
    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>Command</th>
                    <th>Purpose</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><kbd>POST</kbd></td>
                    <td>Create objects or submit data</td>
                </tr>
                <tr>
                    <td><kbd>GET</kbd></td>
                    <td>Fetch objects</td>
                </tr>
                <tr>
                    <td><kbd>PUT</kbd></td>
                    <td>Update an object</td>
                </tr>
                <tr>
                    <td><kbd>DELETE</kbd></td>
                    <td>Remove an object</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection