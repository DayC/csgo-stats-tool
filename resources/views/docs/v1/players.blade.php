@extends('layouts/app')
@section('content')
<div id="api-v1-players" class="col-9 body-content">
<h1>Players</h1>
<p>Players represent individual players within the demo. 
    Each player comes with their own set of base stats (KDA) and more detailed elements such as guns fired and grenades thrown.</p>

<h3>View all players</h3>
<hr/>
<p><small>Fetches all of the players associated with a demo.</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/players</code></pre>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1} '{URL}/api/v1/players'</pre>
<h6>Response | Example</h6>
<pre>
[
{
    "id": 1,
    "name": "TeqR! Nodawe",
    "player_id": 5,
    "guid": "{GUID}",
    "kills": 25,
    "deaths": 29,
    "assists": 3,
    "team_id": 2,
    "demo_id": 1,
    "guns_fired": [
        {
            "weapon": "weapon_hkp2000",
            "amount": 64
        },
        {
            "weapon": "weapon_m4a1_silencer",
            "amount": 108
        },
        {
            "weapon": "weapon_knife",
            "amount": 14
        },
        {
            "weapon": "weapon_knife_t",
            "amount": 23
        },
        {
            "weapon": "molotov",
            "amount": 6
        },
        {
            "weapon": "weapon_glock",
            "amount": 49
        },
        {
            "weapon": "weapon_tec9",
            "amount": 62
        },
        {
            "weapon": "weapon_ak47",
            "amount": 237
        },
        {
            "weapon": "weapon_ump45",
            "amount": 17
        },
        {
            "weapon": "weapon_p250",
            "amount": 14
        },
        {
            "weapon": "weapon_famas",
            "amount": 23
        },
        {
            "weapon": "weapon_m4a1",
            "amount": 13
        },
        {
            "weapon": "weapon_cz75a",
            "amount": 3
        }
    ],
    "grenades_thrown": [
        {
            "grenade": "flashbang",
            "amount": 14
        },
        {
            "grenade": "hegrenade",
            "amount": 2
        },
        {
            "grenade": "decoy",
            "amount": 0
        },
        {
            "grenade": "incgrenade",
            "amount": 4
        },
        {
            "grenade": "smokegrenade",
            "amount": 15
        }
    ]
} ...
]
</pre>

<h3>View players | By ID</h3>
<hr/>
<p><small>Fetches players based an array of ids.</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/players/id</code></pre>
<h6>Parameters</h6>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>demo_id<br/><span class="required">Required</span></td>
            <td>number</td>
            <td>The unique identifier of your demo</td>
        </tr>
        <tr>
            <td>ids<br/><span class="required">Required</span></td>
            <td>array | numbers</td>
            <td>An array of unique player identifiers.</td>
        </tr>
    </tbody>
</table>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1, "ids":[1,2,3]} '{URL}/api/v1/players/id'</pre>

<h3>View players | By Player ID</h3>
<hr/>
<p><small>Fetches players based an array of player ids, the id of the player in the game.</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/players/player_id</code></pre>
<h6>Parameters</h6>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>demo_id<br/><span class="required">Required</span></td>
            <td>number</td>
            <td>The unique identifier of your demo</td>
        </tr>
        <tr>
            <td>player_ids<br/><span class="required">Required</span></td>
            <td>array | numbers</td>
            <td>An array of unique player identifiers.</td>
        </tr>
    </tbody>
</table>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1, "player_ids":[1,2,3]} '{URL}/api/v1/players/id'</pre>

</div>
@endsection