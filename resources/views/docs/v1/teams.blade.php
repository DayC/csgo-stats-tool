@extends('layouts/app')
@section('content')
<div id="api-v1-teams" class="col-9 body-content">
<h1>Teams</h1>
<p>Teams represent groups of players in the game providing grouped stats and play by play breakdowns.</p>

<h3>View all teams</h3>
<hr/>
<p><small>Fetches all of the teams associated with a demo.</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/teams</code></pre>
<h6>Parameters</h6>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>demo_id<br/><span class="required">Required</span></td>
            <td>number</td>
            <td>The unique identifier of your demo</td>
        </tr>
    </tbody>
</table>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1} '{URL}/api/v1/teams'</pre>
<h6>Response | Example</h6>
<pre>
[
	{
		"id": 1,
		"name": "Entropia Blue",
		"first_half": 9,
		"second_half": 6,
		"total": 17
	},
	{
		"id": 2,
		"name": "TeqR esports",
		"first_half": 6,
		"second_half": 9,
		"total": 18
	}
]
</pre>

<h3>View Rounds</h3>
<hr/>
<p><small>Fetches round data</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/teams/rounds</code></pre>
<h6>Parameters</h6>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>demo_id<br/><span class="required">Required</span></td>
            <td>number</td>
            <td>The unique identifier of your demo</td>
        </tr>
        <tr>
            <td>team_ids<br/><span class="required">Required</span></td>
            <td>array | numbers</td>
            <td>An array of unique team identifiers.</td>
        </tr>
    </tbody>
</table>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1, "team_ids":[1,2,3]} '{URL}/api/v1/teams/rounds'</pre>
<h6>Response | Example</h6>
<pre>
{
	"14": [
		{
			"id": 1,
			"round": 14,
			"score": 0,
			"side": "t",
			"overtime": 0,
			"team_id": 1
		},
		{
			"id": 18,
			"round": 14,
			"score": 9,
			"side": "ct",
			"overtime": 1,
			"team_id": 1
		},
		{
			"id": 33,
			"round": 14,
			"score": 15,
			"side": "t",
			"overtime": 1,
			"team_id": 1
		},
		{
			"id": 40,
			"round": 14,
			"score": 0,
			"side": "ct",
			"overtime": 0,
			"team_id": 2
		},
		{
			"id": 57,
			"round": 14,
			"score": 5,
			"side": "t",
			"overtime": 1,
			"team_id": 2
		},
		{
			"id": 72,
			"round": 14,
			"score": 14,
			"side": "ct",
			"overtime": 1,
			"team_id": 2
		}
    ],
}
</pre>

<h3>View Plays</h3>
<hr/>
<p><small>Fetches individual plays.</small></p>
<pre><kbd>GET</kbd> <code>/api/v1/teams/rounds/plays</code></pre>
<h6>Parameters</h6>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>demo_id<br/><span class="required">Required</span></td>
            <td>number</td>
            <td>The unique identifier of your demo</td>
        </tr>
        <tr>
            <td>team_ids<br/><span class="required">Required</span></td>
            <td>array | numbers</td>
            <td>An array of unique team identifiers.</td>
        </tr>
    </tbody>
</table>
<h6>Sample | cURL</h6>
<pre>curl -XGET -H 'Accept: application/json' -H 'Authorization: Bearer {API-KEY}' -H "Content-type: application/json" -d '{"demo_id": 1, "team_ids":[1,2,3]} '{URL}/api/v1/teams/rounds/plays'</pre>
<h6>Response | Example</h6>
<pre>
[
	{
		"id": 1,
		"name": "TeqR! Nodawe",
		"player_id": 5,
		"guid": "{GUID}",
		"kills": 25,
		"deaths": 29,
		"assists": 3,
		"team_id": 2,
		"demo_id": 1,
		"guns_fired": [
			{
				"weapon": "weapon_hkp2000",
				"amount": 64
			},
			{
				"weapon": "weapon_m4a1_silencer",
				"amount": 108
			},
			{
				"weapon": "weapon_knife",
				"amount": 14
			},
			{
				"weapon": "weapon_knife_t",
				"amount": 23
			},
			{
				"weapon": "molotov",
				"amount": 6
			},
			{
				"weapon": "weapon_glock",
				"amount": 49
			},
			{
				"weapon": "weapon_tec9",
				"amount": 62
			},
			{
				"weapon": "weapon_ak47",
				"amount": 237
			},
			{
				"weapon": "weapon_ump45",
				"amount": 17
			},
			{
				"weapon": "weapon_p250",
				"amount": 14
			},
			{
				"weapon": "weapon_famas",
				"amount": 23
			},
			{
				"weapon": "weapon_m4a1",
				"amount": 13
			},
			{
				"weapon": "weapon_cz75a",
				"amount": 3
			}
		],
		"grenades_thrown": [
			{
				"grenade": "flashbang",
				"amount": 14
			},
			{
				"grenade": "hegrenade",
				"amount": 2
			},
			{
				"grenade": "decoy",
				"amount": 0
			},
			{
				"grenade": "incgrenade",
				"amount": 4
			},
			{
				"grenade": "smokegrenade",
				"amount": 15
			}
		]
    },
]
</pre>
</div>
@endsection