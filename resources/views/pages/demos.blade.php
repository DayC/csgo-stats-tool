@extends('layouts/app')
@section('content')
<div id="demos" class="col-9 body-content">
    
    @foreach ($data as $demo)
        <div class="demo card bg-light col-12" style="margin-bottom:24px; padding:15px;">
            <h4>
                <a href="{{$url}}/demo/{{$demo->id}}">
                    <?php echo base64_decode($demo->stamp); ?>
                </a>
            </h4>
            <p><span><strong>Demo ID:</strong> <?php echo $demo->id; ?> - </span><span><strong>Created on:</strong> <?php echo $demo->created_at; ?></span></p>
        </div>
    @endforeach
    
    @if($pages > 0)
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @for ($i = 1; $i <= $pages; $i++)
                <li class="page-item">
                <a class="page-link" href="{{$url}}/{{$i}}">{{$i}}</a>
                </li>
            @endfor
        </ul>
    </nav>
    @endif
</div>
@endsection