@extends('layouts.auth')

@section('content')
<div class="col-12 col-sm-3 mx-auto login-form">
	<div class="card">
			<h1 class="page-title">Login</h1>
			<form action="{{ route('login') }}" method="POST" enctype="multipart/form-data" id="login" role="form">
				{{ csrf_field() }}
				<div class="form-group col-12">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" data-error="Invalid email address" class="form-control" required/>
					</div>
					
					@if ($errors->has('email'))
						<span class="help-block with-errors">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-12">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" data-error="Invalid password" class="form-control" required/>
					@if ($errors->has('password'))
						<span class="help-block with-errors">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-12 submit">
					<input type="submit" value="Login" class="waves-effect waves-light btn center" />
				</div>
			</form>
			<div class="card-action col-12 d-flex flex-row justify-content-between">
				<a href="{{ route('register') }}" class="register">Register</a>
				<a href="{{ route('password.request') }}" class="forgot">Forgot your password?</a>
			</div>
	</div>
</div>
@endsection
