@extends('layouts.auth')

@section('content')
<div class="col-12 col-sm-3 mx-auto login-form">
	<div class="card">
            <h1 class="page-title">Reset Password</h1>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
			<form action="{{ route('password.email') }}" method="POST" enctype="multipart/form-data" id="login" role="form">
				{{ csrf_field() }}
				<div class="form-group col-12">
					<input type="email" name="email" id="email" placeholder="Email..." value="{{ old('email') }}" class="form-control" data-error="Invalid email address" required/>
					@if ($errors->has('email'))
						<span class="help-block with-errors">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group col-12 submit">
					<input type="submit" value="Send Password Reset Link" class="waves-effect waves-light btn center" />
				</div>
			</form>
			<div class="card-action col-12 d-flex flex-row justify-content-between">
                <a href="{{ route('login') }}" class="login">Login</a>
				<a href="{{ route('register') }}" class="register">Register</a>				
			</div>
	</div>
</div>
@endsection