@extends('layouts.auth')

@section('content')
<div class="col-12 col-sm-3 mx-auto login-form">
	<div class="card">
				<h1 class="page-title">Register</h1>
				<form action="{{ route('register') }}" method="POST" enctype="multipart/form-data" id="register" role="form">
					{{ csrf_field() }}
					<div class="form-group col-12">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" data-error="Invalid email address" value="{{ old('email') }}" required/>
						@if ($errors->has('email'))
								<span class="help-block with-errors">
										<strong>{{ $errors->first('email') }}</strong>
								</span>
						@endif
					</div>
					<div class="form-group col-12">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" required/>
					</div>
					<div class="form-group col-12">
						<label for="password-confirm">Confirm Password</label>
						<input type="password" 
							name="password_confirmation"
							id="password-confirm" 
							data-match="#password" 
							data-match-error="Passwords must match!"
							class="form-control"
							required />
						@if ($errors->has('password'))
							<span class="help-block with-errors">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group col-12">
						<div class="g-recaptcha" data-sitekey="6Ld9wlcUAAAAAI2iwZeXPJ8KWWuHSk14ef7J1i7n"></div>
						@if ($errors->has('g-recaptcha-response'))
							<span class="help-block with-errors">
								<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group columns small-12 submit">
						<input type="submit" value="Register" class="waves-effect waves-light btn center" />
					</div>
				</form>
				<div class="card-action columns small-12">
					<a href="{{ route('login') }}" class="login">Login</a>
				</div>
		</div>
</div>
@endsection