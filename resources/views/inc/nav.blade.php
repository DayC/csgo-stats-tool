@guest
@else

<nav id="nav" class="col col-md-2">
			<!-- Branding Image -->
			<!--<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name', 'Laravel') }}
			</a>-->
			<!-- main menu -->
			<ul class="d-flex flex-column justify-content-start">
				
					<li><a href="{{ route('dashboard') }}" class="d-flex flex-row align-items-center"><i class="material-icons">dashboard</i>Dashboard</a></li>
					<li>
						<a href="{{ route('docs-v1') }}" class="d-flex flex-row align-items-center"><i class="material-icons">folder</i>Documentation</a>
						<ul>
							<li><a data-toggle="collapse" href="#version-one" role="button" class="title d-flex flex-row align-items-center">Version 1</a></li>
							<div class="collapse<?php if(isset($open_menus)){ if(in_array("docs-v1", $open_menus)){ echo ' show'; } } ?>" id="version-one">
								<li><a href="{{ route('docs-v1-demo') }}" class="d-flex flex-row align-items-center">Demo</a></li>
								<li><a href="{{ route('docs-v1-players') }}" class="d-flex flex-row align-items-center">Players</a></li>
								<li><a href="{{ route('docs-v1-teams') }}" class="d-flex flex-row align-items-center">Teams</a></li>
							</div>
						</ul>
					</li>
					<li><a href="{{ route('demos') }}" class="d-flex flex-row align-items-center"><i class="material-icons">cloud_circle</i>demos</a></li>
					<li><a href="{{ route('account') }}" class="d-flex flex-row align-items-center"><i class="material-icons">account_circle</i>Account</a></li>
					<li class="align-self-end">
						<a href="{{ route('logout') }}"
							 onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="d-flex flex-row align-items-center"><i class="material-icons">exit_to_app</i>Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
					</li>					
			</ul>

			<!-- burger icon -->
			<!--<a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>-->
</nav>
@endguest