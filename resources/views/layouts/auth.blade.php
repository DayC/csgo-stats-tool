<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="login">
    <div id="index" class="small-12 columns body-content">
        <div class="small-11 large-4 small-centered columns login-form">
					<div class="login-logo columns small-12">
						<img src="{{asset('img/logo-fox.png')}}"
						srcset="{{asset('img/logo-fox.png')}} 1x, {{asset('img/logo-fox@2x.png')}} 2x, {{asset('img/logo-fox@3x.png')}} 3x" />
					</div>
        </div>
        
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
