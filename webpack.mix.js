let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/scss/app.scss', 'public/css')
    .webpackConfig({
        node: {
            console: false,
            global: true,
            process: true,
            __filename: "mock",
            __dirname: true,
            Buffer: true,
            setImmediate: true
        },
        context: path.resolve(__dirname, "public")
    })
    .copyDirectory('node_modules/buffer', 'public/js/vendor/buffer')
    .copyDirectory('node_modules/demofile', 'public/js/vendor/demofile');