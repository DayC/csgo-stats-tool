<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Pull in auth routes
Auth::routes();

/** Pages **/
// Public Group
Route::get('/', 'PagesController@index');

// Apply new register endpoint
Route::post('/register', 'Auth\RegisterController@register')->name('register');

// Dashboard route and controller created on auth init - leave as is for now
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// Display account page - will show api key etc
Route::get('/account', 'PagesController@account')->name('account');
Route::get('/account/token', 'PagesController@apiToken');

// API Documentation
Route::get('/api/v1/docs', 'Docs\DocControllerVersionOne@index')->name('docs-v1');
Route::get('/api/v1/docs/demo', 'Docs\DocControllerVersionOne@demo')->name('docs-v1-demo');
Route::get('/api/v1/docs/players', 'Docs\DocControllerVersionOne@players')->name('docs-v1-players');
Route::get('/api/v1/docs/teams', 'Docs\DocControllerVersionOne@teams')->name('docs-v1-teams');

// Demos List
Route::get('/demos/{count?}', 'PagesController@demos')->name('demos');
Route::get('/demos/demo/{id}', 'PagesController@demo')->name('demo');

/** Functional **/
// Create demo
Route::post('/create-demo', 'DemoController@createDemo')->name('create-demo');
