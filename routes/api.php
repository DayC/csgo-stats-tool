<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
   return $request->user();
})->middleware('auth:api');

// Demos
Route::get('/v1/demo/all', 'API\v1\DemoApiController@getDemos');
Route::get('/v1/demo/name', 'API\v1\DemoApiController@getDemoByName');
Route::get('/v1/demo/id', 'API\v1\DemoApiController@getDemoById');

// Teams
Route::get('/v1/teams', 'API\v1\TeamsApiController@getTeamsByDemoId');
Route::get('/v1/teams/rounds', 'API\v1\TeamsApiController@getTeamRounds');
Route::get('/v1/teams/rounds/plays', 'API\v1\TeamsApiController@getPlays');

// Players
Route::get('/v1/players', 'API\v1\PlayersApiController@getPlayersByDemoId');
Route::get('/v1/players/id', 'API\v1\PlayersApiController@getPlayersById');
Route::get('/v1/players/player_id', 'API\v1\PlayersApiController@getPlayersByPlayerId');